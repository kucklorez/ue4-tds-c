// Copyright Epic Games, Inc. All Rights Reserved.

#include "tdsGameMode.h"
#include "tdsPlayerController.h"
#include "tds/Character/tdsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AtdsGameMode::AtdsGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AtdsPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}